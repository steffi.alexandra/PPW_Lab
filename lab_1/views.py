from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Steffi Alexandra' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 7, 2) #TODO Implement this, format (Year, Month, Date)
npm = 1706043992 # TODO Implement this
hobby = 'eating, gaming, and listening to music'
about_me = 'I am the oldest daughter of two. I was born in Jakarta, and I have been living there since forever. I chose this major merely because both of my parents asked me to, I originally was planning to choose Japanese Literature as my major since I wanted to go there to study after I graduate. I have been playing the violin since I was 5, and I play online games, typically mmorpgs all the time.'

#Information of my chairmates
tio_name = 'Rahmadian Tio'
tio_birthdate = date(1999, 6, 3)
tio_npm = 1706044074
tio_hobby = 'watching film'
about_tio = 'Aku Rahmadian Tio Pratama biasanya di panggil Tio, entah kenapa aku selalu dibilang imut padahal ada yang lebih imut dibanding aku'

wibi_name = 'Hermawan Wibisana'
wibi_birthdate = date(1999, 5, 26)
wibi_npm = 1706043405
wibi_hobby = 'coding'
about_wibi = 'Gua Hermawan Wibisana Arifin, hobi gua main basket and main dota. Gua masuk fasilkom karena mental dari pilihan pertama??jujur aja gua gaada niatan masuk fasilkom samsek wkwk. Dulu gua pengen banget masuk teknik mesin ui. Udah ampe janjian ama temen. Tiap hari belajar sbm sama temen itu. Btw gua baru belajar buat sbm 2 minggu sebelum sbm wkwk. Mungkin karena kurang belajar jadi mental dari mesin. gua milih sistem informasi buat jadi pilihan kedua gua. Kenapa? Karena passing grade SI lebih rendah daripada mesin dan prospek kerjanya banyak. Pas kelar sbm udah seneng seneng kan tuh ama temen. Soalnya bisa ngerjain lumayan banyak. Udah ngomong \"fix mesin sih" eh pas pengumuman Allah Swt. Berkehendak lain:) jadilah gua masuk ke fasilkom. Yaudah lah ya gua berusaha mencintai fasilkom aja (love) kalo mau sbm lagi tahun depan udah males belajar lagi coy males mabim juga wkwk.'

# Create your views here.
# Adding all the info into the dictionary
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'hobby': hobby, 'about_me': about_me,
	'tio_name': tio_name, 'tio_birthdate': calculate_age(tio_birthdate.year), 'tio_npm': tio_npm, 'tio_hobby': tio_hobby, 'about_tio': about_tio,
	'wibi_name': wibi_name, 'wibi_birthdate': wibi_birthdate, 'wibi_npm': wibi_npm, 'wibi_hobby': wibi_hobby, 'about_wibi': about_wibi}
    return render(request, 'index_lab1.html', response)

# A function to calculate someone's age
def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
